<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function utama() {
        return view('welcome');
    }

    public function bio() {
        return view('halaman.register');
    }

    public function kirim(Request $request) {
      $nama = $request['nama'];
      $last = $request['last'];
        return view('halaman.home',['nama' => $nama, 'last'=>$last]);
    }
}
