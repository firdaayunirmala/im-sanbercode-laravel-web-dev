<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
 
        return view('cast.indexcast', ['cast' => $cast]);
    }

    public function tambah(){
        return view('cast.tambahcast');
    }

    public function store(Request $request){
        
        $request = $request->validate([
            'nama' => 'required|',
            'umur' => 'required',
            'bio' => 'required',
        ]);
     
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.showcast', compact('cast'));
    }

    public function edit($id) {
        $cast = DB::table('cast')->find($id);
        return view('cast.editcast', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $request = DB::table('cast')
            ->where('id', $id)
            ->update([ //kolom yang mau diambil
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id) {
        $cast = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}


