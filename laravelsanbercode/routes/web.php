<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AuthController::class, 'utama']); // pertama kali dijalankan 

Route::get('/register',[AuthController::class, 'bio']);
Route::post('/home',[AuthController::class, 'kirim']);

Route::get('/data-table',[HomeController::class, 'table']);
Route::get('/table',[HomeController::class, 'datatable']);

Route::get('/cast',[CastController::class, 'index']); // untuk menampilkan seluruh data
Route::get('/cast/create',[CastController::class, 'tambah']); // untuk menambah data
Route::post('/cast',[CastController::class, 'store']); // untuk menyimpan data baru
Route::get('/cast/{cast_id}',[CastController::class, 'show']); // untuk menampilkan data berdasarkan id tertentu
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']); // untuk mengedit data berdasarkan id tertentu
Route::put('/cast/{cast_id}',[CastController::class, 'update']); // untuk mengupdate data berdasarkan id tertentu
Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']); // untuk mengupdate data berdasarkan id tertentu

