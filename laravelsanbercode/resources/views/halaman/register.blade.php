@extends('layout.master')

@section('judul')
Halaman Register
@endsection

@section('content')
    <form action="/home" method="post">
@csrf 
        <h4>Sign Up Form</h4>
        <label>First Name : </label> <br>
        <input type="text" name="nama"><br><br>
        <label>Last Name : </label><br>
        <input type="text" name="last"><br>
        <p>Gender : </p>
        <input type="radio" value="1" name="gender">Male <br>
        <input type="radio" value="2" name="gender">Female <br>
        <input type="radio" value="3" name="gender">Other <br>
    <br>
        <label>Nationality : </label>
        <select name="nationality">
            <option value="Indonesia" value="1" name="Nationality">Indonesian</option>
            <option value="English" value="2" name="Nationality">English</option>
            <option value="Other" value="3" name="Nationality">Other</option>
        </select>
    
        <p>Language Spoken : </p> 
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br>
    
        <p>Bio</p>
        <textarea name="textarea" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">    
    </form>
@endsection